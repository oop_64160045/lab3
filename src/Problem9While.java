import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int number = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Input Number : ");
        number = sc.nextInt();
        int i = 1;
        while (i <= number) {
            int j = 1;
            while ( j <= number) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
    }
}
}